import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import IFighter from './models/fighterModel';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement = document.getElementById('root') as HTMLElement;
  static loadingElement: HTMLElement = document.getElementById('loading-overlay') as HTMLElement;

  async startApp(): Promise<void> {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters: Array<IFighter> = await fighterService.getFighters();
      const fightersElement: HTMLElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;