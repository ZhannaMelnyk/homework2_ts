import { callFightersApi, callFighterDetailsApi } from '../helpers/apiHelper';
import IFighter from '../models/fighterModel';
import IFighterDetails from '../models/fighterDetailsModel';

class FighterService {
  async getFighters(): Promise<Array<IFighter>> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: Array<IFighter> = await callFightersApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: IFighterDetails = await callFighterDetailsApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
