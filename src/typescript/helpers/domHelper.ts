import IHtmlElement from "../models/htmlElementModel";

export function createElement(element: IHtmlElement): HTMLElement {
  const newElement: HTMLElement = document.createElement(element.tagName);

  if (element.className) {
    const classNames: Array<string> = element.className.split(' ').filter(Boolean);
    newElement.classList.add(...classNames);
  };

  for (let [key, value] of element.attributes) {
    newElement.setAttribute(key, value);
  }

  return newElement;
}