import IFighterDetails from "../models/fighterDetailsModel";
import IAdditionalFighterDetails from "../models/additionalFighterDetailsModel";

export type createSelector = (event: MouseEvent, name: string) => Promise<void>;

export type apiMethods = 'GET' | 'POST' | 'PUT' | 'DELETE';

export type FighterPosition = 'right' | 'left';

export type Pair<key, value> = [key, value];

export type onCloseFunc = () => void;

export type ExtendedFighterDetails = IFighterDetails & IAdditionalFighterDetails;