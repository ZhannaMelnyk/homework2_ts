import { fightersDetails, fighters } from './mockData';
import IFighter from '../models/fighterModel';
import IFighterDetails from '../models/fighterDetailsModel';
import { apiMethods } from './customTypes';

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callFightersApi(endpoint: string, method: apiMethods): Promise<Array<IFighter>> {
  const url: string = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallFightersApi()
    : fetch(url, options)
      .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
      .then((result) => JSON.parse(atob(result.content)))
      .catch((error) => {
        throw error;
      });
}

async function callFighterDetailsApi(endpoint: string, method: apiMethods): Promise<IFighterDetails> {
  const url: string = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallFighterDetailsApi(endpoint)
    : fetch(url, options)
      .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
      .then((result) => JSON.parse(atob(result.content)))
      .catch((error) => {
        throw error;
      });
}

async function fakeCallFightersApi(): Promise<Array<IFighter>> {
  const response: Array<IFighter> = fighters;

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

async function fakeCallFighterDetailsApi(endpoint: string): Promise<IFighterDetails> {
  const response: IFighterDetails = getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighterDetails {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return fightersDetails.find(item => item._id === id) as IFighterDetails;
}

export { callFightersApi, callFighterDetailsApi };
