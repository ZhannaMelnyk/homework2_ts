import IFighterDetails from "../../models/fighterDetailsModel";
import { showModal } from "./modal";
import { createFighterImage } from "../fighterPreview";

export function showWinnerModal(fighter: IFighterDetails) {
  // call showModal function 
  showModal({
    title: `${fighter.name} is winner`,
    bodyElement: createFighterImage(fighter),
    onClose: () => { }
  })
}
