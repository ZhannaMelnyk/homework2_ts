import { createElement } from '../../helpers/domHelper';
import IModal from '../../models/modalModels';
import { onCloseFunc } from '../../helpers/customTypes';

export function showModal(modalParams: IModal): void {
  const root: HTMLElement = getModalContainer();
  const modal: HTMLElement = createModal(modalParams);

  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal(modalParams: IModal): HTMLElement {
  const layer: HTMLElement = createElement({
    tagName: 'div',
    className: 'modal-layer',
    attributes: []
  });
  const modalContainer: HTMLElement = createElement({
    tagName: 'div',
    className: 'modal-root',
    attributes: []
  });
  const header: HTMLElement = createHeader(modalParams.title, modalParams.onClose);

  modalContainer.append(header, modalParams.bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: onCloseFunc): HTMLElement {
  const headerElement: HTMLElement = createElement({
    tagName: 'div',
    className: 'modal-header',
    attributes: []
  });
  const titleElement: HTMLElement = createElement({
    tagName: 'span',
    className: '',
    attributes: []
  });
  const closeButton: HTMLElement = createElement({
    tagName: 'div',
    className: 'close-btn',
    attributes: []
  });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal: Element = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
