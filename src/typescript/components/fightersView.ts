import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { Pair } from '../helpers/customTypes';
import IFighter from '../models/fighterModel';
import { createSelector } from '../helpers/customTypes';

export function createFighters(fighters: Array<IFighter>): HTMLElement {
  const selectFighter: createSelector = createFightersSelector();
  const container: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighters___root',
    attributes: []
  });
  const preview: HTMLElement = createElement({
    tagName: 'div',
    className: 'preview-container___root',
    attributes: []
  });
  const fightersList: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighters___list',
    attributes: []
  });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: IFighter, selectFighter: createSelector): HTMLElement {
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighters___fighter',
    attributes: []
  });
  const imageElement: HTMLElement = createImage(fighter);
  const onClick = (event: MouseEvent) => {
    selectFighter(event, fighter._id)
  };

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributesPairs: Array<Pair<string, string>> = [
    ['src', source],
    ['title', name],
    ['alt', name]
  ]
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes: attributesPairs
  });

  return imgElement;
}