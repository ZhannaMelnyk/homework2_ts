import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
const versusImg = require('../../../resources/versus.png');
import { createFighterPreview } from './fighterPreview';
import IFighterDetails from '../models/fighterDetailsModel';
import { createSelector } from '../helpers/customTypes';
import { fighterService } from '../services/fightersService';

export function createFightersSelector(): createSelector {
  let selectedFighters: Array<IFighterDetails> = [];

  return async (event: MouseEvent, fighterId) => {
    const fighter: IFighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: IFighterDetails = playerOne ?? fighter;
    const secondFighter: IFighterDetails = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map<string, IFighterDetails>();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap

  if (fighterDetailsMap.get(fighterId) == undefined) {
    await fighterService.getFighterDetails(fighterId)
      .then(response => {
        fighterDetailsMap.set(response._id, response);
      });
  }

  return fighterDetailsMap.get(fighterId) as IFighterDetails;
}

function renderSelectedFighters(selectedFighters: Array<IFighterDetails>): void {
  const fightersPreview: HTMLElement = document.querySelector('.preview-container___root') as HTMLElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container: HTMLElement = createElement({
    tagName: 'div',
    className: 'preview-container___versus-block',
    attributes: []
  });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: [
      ['src', versusImg]
    ],
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
    attributes: []
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<IFighterDetails>): void {
  renderArena(selectedFighters);
}
