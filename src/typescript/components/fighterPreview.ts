import { createElement } from '../helpers/domHelper';
import IFighterDetails from '../models/fighterDetailsModel';
import { Pair } from '../helpers/customTypes';
import { FighterPosition } from '../helpers/customTypes';

export function createFighterPreview(fighter: IFighterDetails, position: FighterPosition): HTMLElement {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
    attributes: []
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter != null) {
    const fighterName: HTMLElement = createElement({
      tagName: 'h2',
      className: 'fighter_name',
      attributes: []
    })
    fighterName.innerText = `${fighter.name}`;

    const fighterDetails: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter_details',
      attributes: []
    })

    const fighterHealth: HTMLElement = createElement({
      tagName: 'p',
      className: '',
      attributes: []
    })
    fighterHealth.innerText = `Health: ${fighter.health}`;

    const fighterAttack: HTMLElement = createElement({
      tagName: 'p',
      className: '',
      attributes: []
    })
    fighterAttack.innerText = `Attack: ${fighter.attack}`;

    const fighterDefense: HTMLElement = createElement({
      tagName: 'p',
      className: '',
      attributes: []
    })
    fighterDefense.innerText = `Defense: ${fighter.defense}`;

    fighterDetails.append(fighterHealth, fighterAttack, fighterDefense);

    const fighterImg: HTMLElement = createFighterImage(fighter);

    fighterElement.append(fighterName, fighterDetails, fighterImg);
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails): HTMLElement {
  const { source, name } = fighter;
  const attributesPairs: Array<Pair<string, string>> = [
    ['src', source],
    ['title', name],
    ['alt', name]
  ]

  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes: attributesPairs
  });

  return imgElement;
}
