import { controls } from '../../constants/controls';
import IFighterDetails from '../models/fighterDetailsModel';
import { ExtendedFighterDetails } from '../helpers/customTypes';

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<IFighterDetails> {
  const leftPlayer: ExtendedFighterDetails = {
    ...firstFighter,
    position: 'left',
    currentHealth: firstFighter.health,
    hasBlock: false,
    hasCriticalHit: true,
    isWinner: false
  }

  const rightPlayer: ExtendedFighterDetails = {
    ...secondFighter,
    position: 'right',
    currentHealth: secondFighter.health,
    hasBlock: false,
    hasCriticalHit: true,
    isWinner: false
  }

  const detectorKeysSet = new Set<string>();
 
  function attackHandler(event: KeyboardEvent): void {
    detectorKeysSet.add(event.code);
    checkAction(leftPlayer, rightPlayer, detectorKeysSet);
  };

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', attackHandler);
    document.addEventListener('keyup', () => {
      detectorKeysSet.clear()

      if (leftPlayer.isWinner) {
        document.removeEventListener('keydown', attackHandler);
        resolve(firstFighter);
      } else if (rightPlayer.isWinner) {
        document.removeEventListener('keydown', attackHandler);
        resolve(secondFighter);
      }
    });
  });
}

function checkAction(firstFighter: ExtendedFighterDetails, secondFighter: ExtendedFighterDetails, detectorKeysSet: Set<string>): void {
  switch (detectorKeysSet.size > 0) {
    case detectorKeysSet.has(controls.PlayerOneAttack as string):
      if (!firstFighter.hasBlock) {
        attack(firstFighter, secondFighter);
      }
      break;
    case detectorKeysSet.has(controls.PlayerTwoAttack as string):
      if (!secondFighter.hasBlock) {
        attack(secondFighter, firstFighter);
      }
      break;
    case detectorKeysSet.has(controls.PlayerOneBlock as string):
      block(firstFighter);
      break;
    case detectorKeysSet.has(controls.PlayerTwoBlock as string):
      block(secondFighter);
      break;
    case isCriticalHit(controls.PlayerOneCriticalHitCombination as Array<string>, detectorKeysSet):
      if (firstFighter.hasCriticalHit) {
        criticalHitAttack(firstFighter, secondFighter);
      }
      break;
    case isCriticalHit(controls.PlayerTwoCriticalHitCombination as Array<string>, detectorKeysSet):
      if (secondFighter.hasCriticalHit) {
        criticalHitAttack(secondFighter, firstFighter);
      }
      break;
  }
}

function attack(attacker: ExtendedFighterDetails, defender: ExtendedFighterDetails): void {
  const damage: number = defender.hasBlock ? 0 : getDamage(attacker, defender);
  defender.currentHealth = defender.currentHealth - damage;

  changeHealthIndicator(defender);

  isGameFinished(attacker, defender);
}

function changeHealthIndicator(fighter: ExtendedFighterDetails): void {
  const healthIndicator: HTMLElement = document.getElementById(`${fighter.position}-fighter-indicator`) as HTMLElement;
  const newIndicatorWidth: number = fighter.currentHealth >= 0
    ? (fighter.currentHealth * 100) / fighter.health
    : 0;
  healthIndicator.style.width = `${newIndicatorWidth}%`;
}

function block(fighter: ExtendedFighterDetails): void {
  fighter.hasBlock = !fighter.hasBlock;
  toggleBlockIndicator(fighter);
}

function toggleBlockIndicator(fighter: ExtendedFighterDetails): void {
  const blockIndicator: HTMLElement = document.getElementById(`${fighter.position}-fighter-block-indicator`) as HTMLElement;
  fighter.hasBlock
    ? blockIndicator.style.display = 'block'
    : blockIndicator.style.display = 'none';
}

function isCriticalHit(controls: Array<string>, detectorKeysSet: Set<string>): boolean {
  for (let control of controls) {
    if (!detectorKeysSet.has(control)) {
      return false;
    }
  }
  return true;
}

function criticalHitAttack(attacker: ExtendedFighterDetails, defender: ExtendedFighterDetails): void {
  const criticalHitPower: number = attacker.attack * 2;
  defender.currentHealth = defender.currentHealth - criticalHitPower;
  attacker.hasCriticalHit = false;
  setTimeout(() => attacker.hasCriticalHit = true, 10000);

  changeHealthIndicator(defender);

  isGameFinished(attacker, defender);
}

function isGameFinished(attacker: ExtendedFighterDetails, defender: ExtendedFighterDetails): void {
  if (defender.currentHealth <= 0) {
    attacker.isWinner = true;
  }
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails): number {
  // return damage
  const damage: number = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: IFighterDetails): number {
  // return hit power
  const criticalHitChance: number = Math.random() + 1;
  const power: number = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: IFighterDetails): number {
  // return block power
  const dodgeChance: number = Math.random() + 1;
  const power: number = fighter.defense * dodgeChance;
  return power;
}
