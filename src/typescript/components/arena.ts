import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import IFighterDetails from '../models/fighterDetailsModel';
import { FighterPosition } from '../helpers/customTypes'
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';

export function renderArena(selectedFighters: Array<IFighterDetails>): void {
  const root: HTMLElement = document.getElementById('root') as HTMLElement;
  const arena: HTMLElement = createArena(selectedFighters);

  if (root) {
    root.innerHTML = '';
    root.append(arena);
  }

  // todo:
  // - start the fight
  // - when fight is finished show winner
  const firstFighter: IFighterDetails = selectedFighters[0];
  const secondFighter: IFighterDetails = selectedFighters[1];

  fight(firstFighter, secondFighter)
    .then(winner => showWinnerModal(winner));
}

function createArena(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const arena: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___root',
    attributes: []
  });

  const firstFighter: IFighterDetails = selectedFighters[0];
  const secondFighter: IFighterDetails = selectedFighters[1];

  const healthIndicators: HTMLElement = createHealthIndicators(firstFighter, secondFighter);
  const fighters: HTMLElement = createFighters(firstFighter, secondFighter);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails) {
  const healthIndicators: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___fight-status',
    attributes: []
  });
  const versusSign: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___versus-sign',
    attributes: []
  });
  const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: FighterPosition): HTMLElement {
  const { name } = fighter;
  const container: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___fighter-indicator',
    attributes: []
  });
  const fighterName: HTMLElement = createElement({
    tagName: 'span',
    className: 'arena___fighter-name',
    attributes: []
  });
  const indicator: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___health-indicator',
    attributes: []
  });
  const bar: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: [
      ['id', `${position}-fighter-indicator`]]
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails): HTMLElement {
  const battleField: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___battlefield`,
    attributes: []
  });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
  const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: FighterPosition): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const fighterBlockIndicator: HTMLElement = createBlockIndicator(position);

  const positionClassName: string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
    attributes: []
  });

  fighterElement.append(fighterBlockIndicator, imgElement);
  return fighterElement;
}

function createBlockIndicator(position: FighterPosition): HTMLElement {
  const blockIndicator: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___fighter-block-indicator',
    attributes: [
      ['id', `${position}-fighter-block-indicator`]]
  })

  blockIndicator.innerText = 'block';

  return blockIndicator;
}
