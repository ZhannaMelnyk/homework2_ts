import { onCloseFunc } from "../helpers/customTypes";

export default interface IModal {
  title: string;
  bodyElement: HTMLElement;
  onClose: onCloseFunc;
}