import { FighterPosition } from "../helpers/customTypes";

export default interface IAdditionalFighterDetails {
  position: FighterPosition;
  currentHealth: number;
  hasBlock: boolean;
  hasCriticalHit: boolean;
  isWinner: boolean;
}