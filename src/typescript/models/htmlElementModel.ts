import { Pair } from '../helpers/customTypes';

export default interface IHtmlElement {
  tagName: string;
  className: string;
  attributes: Array<Pair<string, string>>;
}
