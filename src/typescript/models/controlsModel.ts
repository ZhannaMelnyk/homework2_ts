export default interface IControls {
  [index: string]: string | Array<string>;
}